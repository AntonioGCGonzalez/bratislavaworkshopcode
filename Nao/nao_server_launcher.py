import os
import paramiko
import json 
from time import sleep 

def start_nao_server(nao_IP, nao_user, nao_pwd):
    ssh = paramiko.SSHClient()
    ssh.load_host_keys(os.path.expanduser(os.path.join("~", ".ssh", "known_hosts")))
    ssh.connect(nao_IP, username=nao_user, password=nao_pwd, )
    ssh.exec_command('killall nao_server')
    transport = ssh.get_transport()
    channel = transport.open_session()
    pty = channel.get_pty()
    shell = ssh.invoke_shell()
    shell.send("nohup python nao_server.py > /dev/null 2>&1 &\n")
    sleep(2)
    ssh.close()
