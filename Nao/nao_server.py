import socket
from naoqi import ALProxy
from ast import literal_eval
import ctypes 

def change_name(name):
    libc= ctypes.cdll.LoadLibrary("libc.so.6")
    buff = ctypes.create_string_buffer(len(name)+1)
    buff.value= bytes(name)
    libc.prctl(15, ctypes.byref(buff), 0, 0, 0)

change_name("nao_server")

IP = "192.168.0.174"
tts = ALProxy("ALTextToSpeech", IP, 9559)
leds = ALProxy("ALLeds","127.0.0.1",9559)
alive = ALProxy("ALAutonomousLife", "192.168.0.174", 9559)
pose = ALProxy("ALRobotPosture", "192.168.0.174", 9559)
mic = ALProxy("ALAudioRecorder", "192.168.0.174", 9559)

print tts.getAvailableVoices()

def setup():
    tts.setLanguage("English")

def udp_server(host='192.168.0.174', port=12345):
    # Create a UDP socket
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    
    # Bind the socket to the address
    server_address = (host, port)
    print('Starting UDP server')
    sock.bind(server_address)

    while True:
        print('\nWaiting to receive message...')
        data, address = sock.recvfrom(4096)
	print("received data")
        data.decode()
	print data
	command = data.split(":")
	if command[0] == "say":
		tts.say(command[1])

	elif command[0]=="recording":
		if command[1]=="start":
			mic.startMicrophonesRecording("/home/nao/my_audio.wav", "wav", 16000,[0,0,1,0])
		elif command[1]=="stop":
			mic.stopMicrophonesRecording()
	elif command[0] == "setting":
		if command[1]=="language":
			tts.setLanguage(command[2])

		elif command[1]=="pose":
			pose.goToPosture(command[2], 1.0)

		if command[1]=="state":
			alive.setState(command[2])

		elif command[1]=="voice":
			try:
				tts.setVoice(command[2])
			except:
				print("Error, voice not available")
		elif command[1]=="speech_speed":
			tts.setParameter("defaultVoiceSpeed", int(command[2]))
		elif command[1]=="speech_pitch":
			tts.setParameter("pitchShift", float(command[2]))
		elif command[1]=="speech_volume":
			tts.setParameter("vol")
		elif command[1]=="eye_color": 
			if command[2]=="left":
				color = literal_eval(command[3])
				leds.setIntensity("LeftFaceLedsRed", color[0])
				leds.setIntensity("LeftFaceLedsGreen", color[1])
				leds.setIntensity("LeftFaceLedsBlue", color[2])
			elif command[2]=="right":
				color = literal_eval(command[3])
				leds.setIntensity("RightFaceLedsRed", color[0])
				leds.setIntensity("RightFaceLedsGreen", color[1])
				leds.setIntensity("RightFaceLedsBlue", color[2])
		elif command[1]=="foot_color": 
			if command[2]=="left":
				color = literal_eval(command[3])
				leds.setIntensity("LeftFootLedsRed", color[0])
				leds.setIntensity("LeftFootLedsGreen", color[1])
				leds.setIntensity("LeftFootLedsBlue", color[2])
			elif command[2]=="right":
				color = literal_eval(command[3])
				leds.setIntensity("RightFootLedsRed", color[0])
				leds.setIntensity("RightFootLedsGreen", color[1])
				leds.setIntensity("RightFootLedsBlue", color[2])
			
        if data:            
            sent = sock.sendto(data, address)
            print('ping back')

if __name__ == '__main__':
    setup()
    udp_server(host=IP, port=12345)
