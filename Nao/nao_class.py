from time import sleep
import os
import paramiko
from Nao.SpeechRecognitionFromAudio import RecognizeFromFile
from Nao.pc_sender import udp_client
from LLM.dolly_ai import DollyAI
#from LLM.mock_ai import MockAI


class NAO:

    def __init__(self, language = "English", voice="ewa", speed=150, pitch=1, volume=100, initial_state="solitary", initial_posture="Stand", initial_left_eye_color=[0,0,1], initial_right_eye_color=[1,0,0], IP='192.168.0.174', PORT=12345, model_size=3, nao_user="nao", nao_password = "nao"):
    
        self.IP = IP
        self.port = PORT
        self.user = nao_user
        self.password = nao_password
        self.pitch = 1
        self.LLM = DollyAI(model_size) # MockAI()
        self.setup(language,
                   voice,
                   speed,
                   pitch,
                   volume,
                   initial_left_eye_color,
                   initial_right_eye_color,
                   initial_state,
                   initial_posture,)

    def setup(self,language, voice, speed, pitch, volume,l_eye_color,r_eye_color, state, posture):
        self.set_language(language)
        self.set_speed(speed)
        self.set_pitch(pitch)
        self.set_volume(volume)
        self.set_eye_color("left", l_eye_color)
        self.set_eye_color("right",r_eye_color)
        self.set_state(state)
        self.set_posture(posture)

    def speak(self, text):
        udp_client("say:\\vol={}\\".format(self.volume)+text, self.IP, self.port)

    def set_language(self, language):
        udp_client("setting:language:"+language, self.IP, self.port)
        self.language = language
    def get_language(self):
        return self.language

    def set_speed(self, speed):
        udp_client(f"setting:speech_speed:{speed}", self.IP, self.port)
        self.speed = speed
    def get_speed(self):
        return speed

    def set_pitch(self, pitch):
        udp_client(f"setting:speech_pitch:{pitch}", self.IP, self.port)
        self.pitch= pitch
    def get_pitch(self):
        return self.pitch

    def set_volume(self,volume):
        self.volume=volume
    def get_volume(self):
        return self.volume

    def set_eye_color(self,eye_info,color):
        udp_client(f"setting:eye_color:{eye_info}:{color}", self.IP, self.port)
        if "left" in eye_info:
            self.l_eye_color = color
        elif "right" in eye_info:
            self.r_eye_color = color

    def get_eye_color(self,eye_info):
        if "left" in eye_info:
            return self.l_eye_color
        elif "right" in eye_info:
            return self.r_eye_color

    def set_state(self,state):
        try:
            assert state == "disabled" or state == "interactive" or state =="solitary"
            udp_client(f"setting:state:{state}", self.IP, self.port)
            self.state = state
        except:
            print("Error, state should be 'disabled', 'interactive' or 'solitary'")
    def get_state(self):
        return self.state

    def set_posture(self, posture):
        try:
            assert posture in ["Crouch", "LyingBack", "LyingBelly", "Sit",
                               "SitRelax", "Stand", "StandInit", "StandZero"]
            udp_client(f"setting:pose:{posture}", self.IP, self.port)
            self.posture = posture
        except:
            print("Error, posture not found.")
    def get_posture(self):
        return self.posture

    def start_recording(self):
        udp_client(f"recording:start", self.IP, self.port)

    def stop_recording(self):
        udp_client(f"recording:stop", self.IP, self.port)

    def get_audio_file(self):
        ssh = paramiko.SSHClient()
        ssh.load_host_keys(os.path.expanduser(os.path.join("~", ".ssh", "known_hosts")))
        ssh.connect(self.IP, username=self.user, password=self.password)
        sftp = ssh.open_sftp()
        sftp.get("/home/nao/my_audio.wav", "/home/cuda-user/Desktop/my_audio.wav")  # change to your directory
        sftp.close()
        ssh.close()

    def recognize_audio(self):
        text = RecognizeFromFile("/home/cuda-user/Desktop/my_audio.wav")  # change to your directory
        return text

    def generate_response_GPTJ(self, prompt, context):
        return self.LLM.generate_response(prompt, context)
