# Programming for Trust: How Robot Behavior Influences Human Confidence - Repository
###Trust in Human-Robot Interaction Summer School (THRISS)
###Bratislava, Slovakia

##Organizers
###Social Robot Lab, Jagiellonian University
**Main Speaker**: Bipin Indurkhya (bipin.indurkhya(at)uj.edu.pl)
**Assistants**: Alicja Wróbel, Antonio Galiza Cerdeira Gonzalez (antonio.gonzalez(at)uj.edu.pl), Barbara Sienkiewicz

## Description
The present repository contain the code that was used in the experimental section of the "Programming for Trust: How Robot Behavior Influences Human Confidence" workshop. 
It containes necessary code to:

* Deploy opens source large language model DollyAI;

* Mock ai in case you cannot run a DollyAI on your machine;

* A modified version of the Python API to interface with Misty II robot;

* A custom API based on naoqi library for python2 and UDP sockets to control a Nao robot.

## Disclaimers
All code here is presented as is and should be used with caution, since it may cause dammage to your robots, environment, people or animals, as it allows robots to move. It should be used carefully and first tested on safe environment. The repository creators, Social Robotics Lab and Jagiellonian University take no responsability for any damage caused by misuse of the present repository. 

The present DollyAI model is the pre-trained version offered by databricks at Huggin Face. It was trained with internet data and has no filters. Thus, it may generate harmful responses and should be use very carefully in a setting where everyone is informed of this fact and consents on using such an AI chatbot system. Any harmful comments generated by DollyAI are in no way, shape or form endossed by the creators of this source code, Social Robotics lab and Jagiellonian University.

The present source code is in no way shape or form associated with Misty, Aldebaran or Databricks, creators of Misty II, Nao and DollyAI, respectively. 
	
## Requirements

In order to run the present repository, it is necessary to have:

* Computer with a Nvidia GPU with at least 4GB of VRAM (3b parameters model, more is necessary to run the 7b and the 12b models, which can take up to 12GB VRAM);

* Nao Robot;

* Misty II Robot;

* Smartphone with Misty robot App (both Android and iPhone support it)

* Internet Connection; 

* Cuda 11.8; 

* cuDNN 8.7; 

* A Linux distribution that supports Cuda 11.8 and cuDNN8.7, recommended distros: Ubuntu 20.04 or Ubuntu 22.04; 

* ssh;

* Python3 libraries (inside computer): torch, transformers, regex numpy, requests, websocket, SpeechRecognition, paramiko; 

* Python2 libraries (inside Nao): naoqi;

## How to Install

1. In order to install the present repository, it is necessry to first install Cuda 11.8 and cuDNN 8.7. A tutorial for 
Ubuntu 22.04 can be found in [this link](https://gist.github.com/MihailCosmin/affa6b1b71b43787e9228c25fe15aeba).

If you do not intend on running any LLM on your machine, you can skip the step above, but modifications to our code
is necessary to use another LLM API. 

2. Install necessary python3 libraries, by running the following command:

> pip3 install torch transformers regex numpy requests websocket SpeechRecognition paramiko

3. Install ssh to your computer and configure it appropriately. A tutorial can be found [in this link](https://www.cyberciti.biz/faq/how-to-install-ssh-on-ubuntu-linux-using-apt-get/)

4. Discover your Nao IP (press his chest button once), username (normally, nao) and password (default is nao, if you lost your password, [see this link](https://www.aldebaran.com/en/support/nao-6/i-forgot-my-robot-password)).

5. Ssh into your nao robot and install naoqi library in it. A tutorial can be found [in this link](http://doc.aldebaran.com/2-5/dev/python/install_guide.html)

> ssh nao@(your robot IP)

6. Copy file Nao/nao_server.py to your Nao robot using the following command:

> scp (path/to/your/repository)/bratislavaworkshopcode/Nao/nao_server.py nao@(your robot IP):nao_server.py

7. Change the Config.json file and introduce your Nao robot's IP address, user ("NAO_USER") and password ("NAO_PWD"). 
Also, introduce Misty II robot's IP adresses. If you do not know your Misty II robot's IP adress, you can connect to 
it using the Misty app and check it's address. 

You might need to change the Nao server port if the default 12345 port is already in use by another application. 
Remember to change the port in the nao_server.py file to reflect the port you have selected (change the port number 
on line 31).

You can also change the DollyAI model betwee 3b, 7b and 12b by changing the "DOLLY_AI_SIZE" from 3 to 7 or 12, if you 
have a powerful enough GPU.

8. You have finished installing this repository, now it is time to run it.

## Running the Code

While you can run both robots at the same time, it is not recommended since for the current implementation, you will
have 2 instances of DollyAI running in your computer and, thus, will need a lot of VRAM. The present code can be 
improved a lot in that sense if a socket server is created for DollyAI, which allows it to be shared by multiple 
robots. This way, current startup script only supports one robot at a time, but feel free to improve on our curent 
code.

In order to start running the code:

1. Decide between Nao and Misty;

2. Turn on the chosen robot and wait until it finishes booting up;

3. Make sure your computer and the robot is in the same network;

4. If needed, change the configuration file to change IP and other parameters;

5. Execute command:

> ./star.sh (robot name)

<robot name> should either be nao or misty, in lower case. 

## Making the Code your Own. 

The present main files provide examples of the capabilities of the robots, but are not everything it can make. In order
to customize what the robots do, please check the files and change them in order to obtain the desired behavior for 
your experiment. In order to modify Nao's behavior, change script nao_main.py. For misty, modify script misty_main.py. 
There is no extensive documentation for the custom Nao API nor for the modified Misty II's python API, but, by checking
files **nao_class.py** and **mistyPy.py** you can find which functions have been implemented and extend the present 
main scripts.


## Repository Files

### bratislavaworkshopcode

* LICENSE-2.0.txt: Apache 2.0 description file;

* misty_main.py: main script for controlling Misty;

* nao_main.pt: main script for controlling Nao;

* README.md: this present file;

* start.sh: bash script to start selected robot.

### LLM

* __init__.py: exists so directories are callable as packages;

* dolly_ai.py: DollyAI class wrapper;

* instruct_pipeline.py: instruct pipeline;

* mock_ai.py: fake AI for testing.

### Misty

* __init__.py: exists so directories are callable as packages;

* mistyPy.py: Misty II python API;

* SpeechRecognitionAudio.py: Speech recognition code.

### Nao

* __init__.py: exists so directories are callable as packages;

* nao_class.py: Nao interface class wrapper;

* nao_server.py: UDP socket server for Nao;

* nao_server_launcher.py: launches UDP socket server in Nao from computer;

* pc_sender.py: UDP socket client for Computer;

* SpeechRecognitionAudio.py: speech recognition code.

## Support
If you have any inquiries regarding the present repository, 
please contact antonio.gonzalez(at)uj.edu.pl

## Authors and acknowledgment

This repository was programmed by Antonio Galiza Cerdeira Gonzalez.

I would like to acknowledge the developers of Misty's python API, Aldebaran Nao delevopers of the naoqi library and 
Databricks for having created the base libraries necessry for the creation of this repository and for allowing us 
to make a much richer workshop.

## License
This repository is provided under the Apache v2.0 License which is explained in the LICENSE-2.0.txt document.

## Project status
This project is finished, since the workshop was presented and will receive no further updates, it is provided so 
participants can continue their work later withour having to develop all present code all over again.
