#!/usr/bin/env python

###############################################################################
#                                                                             #
#                                                                             #
#                                                                             #
###############################################################################

import torch
from LLM.instruct_pipeline import InstructionTextGenerationPipeline
from transformers import AutoModelForCausalLM, AutoTokenizer

tokenizer = AutoTokenizer.from_pretrained("databricks/dolly-v2-3b", padding_side="left")
model = AutoModelForCausalLM.from_pretrained("databricks/dolly-v2-3b", device_map="auto", torch_dtype=torch.bfloat16)

generate_text = InstructionTextGenerationPipeline(model=model, tokenizer=tokenizer)

#-----------------------------------------------------------------------------#

class DollyAI:

    def __init__(self, size, memory = True, context = None, dtype = torch.bfloat16):
        try:
            assert size == 3
        except AssertionError:
            print("Dolly AI is available in the following sizes, 3, 7 and 12 billion parameters, but this computer can only run the 3b version.")

        model_name = f"databricks/dolly-v2-{size}b"

        tokenizer = AutoTokenizer.from_pretrained("databricks/dolly-v2-3b", padding_side="left")
        model = AutoModelForCausalLM.from_pretrained("databricks/dolly-v2-3b", device_map="auto", torch_dtype=torch.bfloat16)

        self.tokenizer = AutoTokenizer.from_pretrained(model_name, padding_side="left")
        self.model = AutoModelForCausalLM.from_pretrained(model_name,
                                                          device_map="auto",                                                           torch_dtype=dtype)

        self.generate_text = InstructionTextGenerationPipeline(model=self.model,                                                                tokenizer=self.tokenizer)

        self.memory = True # tells wheter the LLM should remember previous queries in conversation.
        self.context = context if context is not None else []

    def generate_response(self, input_text, custom_context=None):
        #print("Current context", self.context)
        if custom_context:
            local_context = custom_context
        else:
            local_context = self.context
        #ChatPromptTemplate.from_template("tell me a joke about {topic}")
        prompt_template = "{}. Context for response: {}."
        prompt_with_context = prompt_template.format(input_text, "\n".join(local_context))
        #prompt_with_context = "{}. Context for response: {}.".format(input_text, "\n".join(local_context))
        response = self.generate_text(prompt_with_context)[0]["generated_text"]
        if self.memory:
            self.store_context(input_text, response[:200])
        return response[:200]

    def store_context(self, query, response):
        self.context.append( "input: "+ query + "\n" + "response: " + str(response) + "\n")
        if len(self.context)>5:
            self.context=self.context[1:]
        # print(self.context)

# dolly_sheep = DollyAI(3)
#
# while 1:
#     query = input(">>>")
#     if query == "exit": break
#     print(dolly_sheep.generate_response(query))
