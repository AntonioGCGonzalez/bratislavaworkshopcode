# Programming for Trust: How Robot Behavior Influences Human Confidence - Repository
###Trust in Human-Robot Interaction Summer School (THRISS)
###Bratislava, Slovakia

##Organizers
###Social Robot Lab, Jagiellonian University
**Main Speaker**: Bipin Indurkhya (bipin.indurkhya(at)uj.edu.pl)
**Assistants**: Alicja Wróbel, Antonio Galiza Cerdeira Gonzalez (antonio.gonzalez(at)uj.edu.pl), Barbara Sienkiewicz

## Description

A modified version of the Python API to interface with Misty II robot;
	
## Requirements

In order to run the present repository, it is necessary to have:

* Computer with a Nvidia GPU with at least 4GB of VRAM (3b parameters model, more is necessary to run the 7b and the 12b models, which can take up to 12GB VRAM);

* Nao Robot;

* Misty II Robot;

* Internet Connection 

* Ubuntu 20.04 or 22.04 

* Cuda 11.8 

* cuDNN 8.7 
(tutorial on how to install both)[https://gist.github.com/MihailCosmin/affa6b1b71b43787e9228c25fe15aeba]

* Python3 libraries (for computer): torch, transformers, logging, re, typing, numpy, json, requests, 
threading, time, websocket, base64, SpeechRecognition, paramiko, os, socket 

* Python2 libraries (for Nao): socket, naoqi

## File structure

--- bratislavaworkshopcode
	|
	|--- LICENSE-2.0.txt: Apache 2.0 description file;
	|
	|--- misty_main.py: main script for controlling Misty;
	|
	|--- nao_main.pt: main script for controlling Nao;
	|
	|--- README.md: this present file;
	|
	|--- start.sh: bash script to start selected robot.
	|
	|--- LLM
	|	 |
	|	 |--- AboutLLM.md: explains how to use LLM
	|	 |
	|	 |--- __init__.py
	|	 |
	|	 |--- dolly_ai.py: DollyAI class wrapper;
	|	 |
	|	 |--- instruct_pipeline.py: instruct pipeline;
	|	 |
	|	 |--- mock_ai.py: fake AI for testing.
	| 
	|--- Misty
	|	 |
	|	 |--- AboutMisty.md: explains how to use Misty;
	|	 |
	|	 |--- __init__.py
	|	 |
	|    |---mistyPy.py: Misty II python API;
	|    |
	|    |---SpeechRecognitionAudio.py: Speech recognition code.
	|
	|--- Nao
	|	 |--- AboutNao.md: explains how to use Nao
	|	 |
	|	 |--- __init__.py
	|	 |
	|	 |--- nao_class.py: Nao interface class wrapper;
	|    |
	|    |--- nao_server.py: UDP socket server for Nao;
	|    |
	|    |--- pc_sender.py: UDP socket client for Computer;
	|    |
	|    |---SpeechRecognitionAudio.py: Speech recognition code.

## Support
If you have any inquiries regarding the present repository, 
please contact antonio.gonzalez(at)uj.edu.pl

## Authors and acknowledgment

This repository was programmed by Antonio Galiza Cerdeira Gonzalez.

I would like to acknowledge the developers of Misty's python API, Aldebaran Nao delevopers of the naoqi library and 
Databricks for having created the base libraries necessry for the creation of this repository and for allowing us 
to make a much richer workshop.

## License
This repository is provided under the Apache v2.0 License which is explained in the LICENSE-2.0.txt document.

## Project status
This project is finished, since the workshop was presented and will receive no further updates, it is provided so 
participants can continue their work later withour having to develop all present code all over again.
