#!/usr/bin/env python


class MockAI:

    def __init__(self, size, memory = True, context = None, dtype = None):
        pass

    def generate_response(self, input_text, custom_context=None):
        return choice(["This is a test AI, I cannot generate anything",
                       "I am a test engine, use a real LLM, please",
                       "Please, use a real LLM, I exist for test purposes"])
