	# /**********************************************************************
	# 	Copyright 2020 Misty Robotics
	# 	Licensed under the Apache License, Version 2.0 (the "License");
	# 	you may not use this file except in compliance with the License.
	# 	You may obtain a copy of the License at
	# 		http://www.apache.org/licenses/LICENSE-2.0
	# 	Unless required by applicable law or agreed to in writing, software
	# 	distributed under the License is distributed on an "AS IS" BASIS,
	# 	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	# 	See the License for the specific language governing permissions and
	# 	limitations under the License.

	# 	**WARRANTY DISCLAIMER.**

	# 	* General. TO THE MAXIMUM EXTENT PERMITTED BY APPLICABLE LAW, MISTY
	# 	ROBOTICS PROVIDES THIS SAMPLE SOFTWARE "AS-IS" AND DISCLAIMS ALL
	# 	WARRANTIES AND CONDITIONS, WHETHER EXPRESS, IMPLIED, OR STATUTORY,
	# 	INCLUDING THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
	# 	PURPOSE, TITLE, QUIET ENJOYMENT, ACCURACY, AND NON-INFRINGEMENT OF
	# 	THIRD-PARTY RIGHTS. MISTY ROBOTICS DOES NOT GUARANTEE ANY SPECIFIC
	# 	RESULTS FROM THE USE OF THIS SAMPLE SOFTWARE. MISTY ROBOTICS MAKES NO
	# 	WARRANTY THAT THIS SAMPLE SOFTWARE WILL BE UNINTERRUPTED, FREE OF VIRUSES
	# 	OR OTHER HARMFUL CODE, TIMELY, SECURE, OR ERROR-FREE.
	# 	* Use at Your Own Risk. YOU USE THIS SAMPLE SOFTWARE AND THE PRODUCT AT
	# 	YOUR OWN DISCRETION AND RISK. YOU WILL BE SOLELY RESPONSIBLE FOR (AND MISTY
	# 	ROBOTICS DISCLAIMS) ANY AND ALL LOSS, LIABILITY, OR DAMAGES, INCLUDING TO
	# 	ANY HOME, PERSONAL ITEMS, PRODUCT, OTHER PERIPHERALS CONNECTED TO THE PRODUCT,
	# 	COMPUTER, AND MOBILE DEVICE, RESULTING FROM YOUR USE OF THIS SAMPLE SOFTWARE
	# 	OR PRODUCT.

	# 	Please refer to the Misty Robotics End User License Agreement for further
	# 	information and full details:
	# 		https://www.mistyrobotics.com/legal/end-user-license-agreement/
	# **********************************************************************/

from Misty.mistyPy import Robot
from time import sleep 
import json 
from LLM.dolly_ai import DollyAI

config = None
with open("Config.json","r") as config_file:
	config = json.load(config_file)
	
config = json.load(config_file)
misty_IP = config["MISTY_IP"]
model_size = config["DOLLY_AI_SIZE"]

misty = Robot(misty_IP)

LLM = DollyAI(model_size)

# RGB values, between 0 and 255
misty.changeLED(0, 100, 0)

#Head roll, pitch and yaw rotations control function.
# -5<=roll<=5, -5<=pitch<=5, -5<=yaw<=5, 0<=speed<=100
misty.moveHeadPosition(0, 0, 0, 100) # center the head

# -90< left_arm_angle<90, -90< right_arm_angle<90, 0<=left_arm_speed<=100, 0<=right_arm_speed<=100
misty.moveArmsDegrees(0, 0, 100, 100)

# -100<=linear_velocity<=100, -100<=angular_velocity<=100, time_in_miliseconds
#misty.driveTime(.0,-30,3000) 

# Misty speak command. Unfortunately, for the time being, it is not possible to
# change Misty's language or voice, only pitch and speech rate. 
# a sleep time is necessary if you want to wait misty to stop talking before
# executing other commands.
misty.Speak("Hello, NOW!", pitch=1, speechRate=1)
sleep(2.0)

# blinking settings, minimum time with eyes closed, maximum time with eyes closed, 
# minimum time with eyes opened, maximum time with eyes opened. All times
#  are is ms. 
misty.setBlinkSettings(1,50,1000,2000)

# set Misty's emotion. Choose one in the following list:[Admiration, Aggressiveness, 
# Amazement, Anger, ApprehensionConcerned, Contempt, ContentLeft, ContentRight, 
# DefaultContent, Disgust, Disoriented, EcstacyHilarious, EcstacyStarryEyed, Fear, 
# Grief, Joy, Joy2, JoyGoofy, JoyGoofy2, JoyGoofy3, Love, Rage, Rage2, Rage3, Rage4, 
# RemorseShame, Sadness, Sleeping, SleepingZZZ, Sleepy, Sleepy2, Sleepy3, Sleepy4, 
# Surprise, Terror, Terror2, TerrorLeft, TerrorRight]
misty.setEyesEmotion("Sleepy")

# Display an image on Misty's display
# image name or url; False if image name, True if url
#misty.setDisplayImage("https://as1.ftcdn.net/v2/jpg/05/44/96/14/1000_F_544961466_nuT79PjfiddndENEVoFO8RGRkYEF7e9z.jpg", True)

#---------------------------------------------------------------------------------#
# Pipeline for using GPT-J, start recording audio, stop recording audio, 
# recognize audio from file, send recognized text to GPT-J, make Misty speak the
# generated response
#---------------------------------------------------------------------------------#

# Start recording audio and stop recording after 5.0 secons. Change time as necessary.
misty.StartRecordingAudio("myRecording.wav")
sleep(5.0)
misty.StopRecordingAudio()
# this sleep time is necessary to ensure that buffered audio frames are properly saved
sleep(5.0)

# uncomment this command inc ase you want to see which audios are available in Misty's
# memory. 
#misty.printAudioList()

# uncomment in case you wish to get Misty to playback the human speech audio that
# was recorder in previous commands.
#misty.playAudio("myRecording.wav")
#sleep(5.0)

heard = misty.SpeechRecognitionFromAudioFile("myRecording.wav")
print(heard)
response = LLM.generate_response(heard)
print(response)
misty.Speak(response, pitch=1, speechRate=1)
sleep(2.0)

#misty.getAudioFile("myRecording.wav")
#misty.uploadAudio("/home/bagre/AntonioHello.wav","myAudio.wav")
sleep(1.0)

