#!/usr/bin/env python3
import speech_recognition as sr

def RecognizeFromFile(filename, language="en-US"):
    recognizer = sr.Recognizer()
    audio = None
    with sr.AudioFile(filename) as source:
        audio = recognizer.record(source)  # read the entire audio file
    try:
        #phrase = recognizer.recognize_sphinx(audio,
        #                                      language=self.language)
        return recognizer.recognize_google(audio,
                                           language=language)
    except sr.UnknownValueError:
        print("PockectShphinx could not understand audio")
        return ""
    except sr.RequestError as e:
        print("Could not request results from PockectShphinx\
            Recognition service; {0}".format(e))
        return ""