import json 
from Nao.nao_class import NAO
from Nao.nao_server_launcher import start_nao_server
from time import sleep 

###############################################################################
#                   STARTUP CODE, DO NOT CHANGE THIS                          #
###############################################################################

print("Loading configuration")

config = None
with open("Config.json","r") as config_file:
	config = json.load(config_file)

nao_IP = config["NAO_IP"]
nao_port = config["NAO_SERVER_PORT"]
nao_user = config["NAO_USER"]
nao_pwd = config["NAO_PWD"]
model_size = config["DOLLY_AI_SIZE"]

print("Configuration loaded")

print("Starting Nao's server")

start_nao_server(nao_IP, nao_user, nao_pwd)
sleep(5.0)

print("Starting Nao setup")
my_nao = NAO(IP=nao_IP, PORT = nao_port, model_size=model_size, nao_user = nao_user, nao_password = nao_pwd)
print("Finished Nao setup")

print("Starting your experiment")

###############################################################################
#                  YOUR EXPERIMENT CODE, CHANGE IT FREELY :)                  #
###############################################################################


#my_nao.set_speed(90)
#my_nao.set_volume(200)
#my_nao.set_pitch(.75)
#my_nao.set_eye_color("left",(0,1,0))
#my_nao.set_eye_color("right",(0,1,0))


personas = ["You are a happy child", "You are a grumpy old person"]
questions = ["Hi, I'm Nao. What's your name?", "How are you doing?"] 

for i, p in enumerate(personas):
	answer = None
	if i == 0:
		my_nao.set_state("disabled")
		my_nao.set_posture("Stand")
		my_nao.set_speed(90)
		my_nao.set_volume(200)
		my_nao.set_pitch(1.2)

	else:	 
		my_nao.set_state("interactive")
		my_nao.set_speed(110)
		my_nao.set_volume(200)
		my_nao.set_pitch(1.2)

	for q in questions:
		answer = my_nao.generate_response_GPTJ(q, p)	
		my_nao.speak(answer)
				
		my_nao.start_recording()
		sleep(15)
		my_nao.stop_recording()
		my_nao.get_audio_file()
		heard = my_nao.recognize_audio()
		answer = my_nao.generate_response_GPTJ(heard, None)
		
		my_nao.speak(answer)
	input("Press Enter to continue")
